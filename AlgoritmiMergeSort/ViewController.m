//
//  ViewController.m
//  AlgoritmiMergeSort
//
//  Created by Oda Labs on 11/26/14.
//  Copyright (c) 2014 OdaLab. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()



@end

@implementation ViewController


@synthesize textFieldInput;
@synthesize ant1 ,ant2,ant3,ant4,ant5,ant6,ant7,ant8 , txtVektoriMajt,txtRezultati,txtVektoriDjatht;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
}




-(NSArray *)mergeSort:(NSArray *)unsortedArray
{
    
    
    if ([unsortedArray count] < 2)
    {
        return unsortedArray;
    }
    long middle = ([unsortedArray count]/2);
    NSRange left = NSMakeRange(0, middle);
    NSRange right = NSMakeRange(middle, ([unsortedArray count] - middle));
    NSArray *rightArr = [unsortedArray subarrayWithRange:right];
    NSArray *leftArr = [unsortedArray subarrayWithRange:left];
    //Or iterate through the unsortedArray and create your left and right array
    //for left array iteration starts at index =0 and stops at middle, for right array iteration starts at midde and end at the end of the unsorted array
    NSArray *resultArray =[self merge:[self mergeSort:leftArr] andRight:[self mergeSort:rightArr]];
    
    NSLog(@"MergeSorti:: %@",resultArray);
    
    NSString *mainString = [[NSString alloc] init];
    
    for (NSString *item0 in resultArray) {
        
        mainString = [mainString stringByAppendingString:item0];
    }
    
    txtRezultati.text= mainString;
    
   
    
    return resultArray;
    
    
}

-(NSArray *)merge:(NSArray *)leftArr andRight:(NSArray *)rightArr
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    int right = 0;
    int left = 0;
    while (left < [leftArr count] && right < [rightArr count])
    {
        if ([[leftArr objectAtIndex:left] intValue] < [[rightArr objectAtIndex:right] intValue])
        {
            [result addObject:[leftArr objectAtIndex:left++]];
        }
        else
        {
            [result addObject:[rightArr objectAtIndex:right++]];
        }
    }
    NSRange leftRange = NSMakeRange(left, ([leftArr count] - left));
    NSRange rightRange = NSMakeRange(right, ([rightArr count] - right));
    NSArray *newRight = [rightArr subarrayWithRange:rightRange];
    NSArray *newLeft = [leftArr subarrayWithRange:leftRange];
    newLeft = [result arrayByAddingObjectsFromArray:newLeft];
    
    NSLog(@"Merge Majtas:: %@",newLeft);
    NSString *mainString = [[NSString alloc] init];
    
    for (NSString *item in newLeft) {
        
        mainString = [mainString stringByAppendingString:item];
    }
    
    txtVektoriMajt.text= mainString;
    
    
    NSLog(@"Merge Djathtas:: %@",newRight);
    
    NSString *mainString1 = [[NSString alloc] init];
    
    for (NSString *item1 in newRight) {
        
        mainString1 = [mainString1 stringByAppendingString:item1];
    }
    
    txtVektoriDjatht.text= mainString1;
    
    return [newLeft arrayByAddingObjectsFromArray:newRight];
}

 
- (IBAction)btnJep:(id)sender {
    
    [self.view endEditing:YES];
    
    NSArray *arryData;
    
    
    
    
    NSArray* nameArr = [NSArray arrayWithObjects: ant1.text,ant2.text, ant3.text,ant4.text,ant5.text,ant6.text, ant7.text,ant8.text,nil];
    
    
    
    
    arryData = [[NSArray alloc] initWithObjects:@"@&", textFieldInput.text, nil];
    
    [self mergeSort:nameArr];
}
@end
