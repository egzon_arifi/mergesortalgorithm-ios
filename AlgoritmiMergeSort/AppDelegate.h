//
//  AppDelegate.h
//  AlgoritmiMergeSort
//
//  Created by Oda Labs on 11/26/14.
//  Copyright (c) 2014 OdaLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

