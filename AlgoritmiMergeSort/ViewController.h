//
//  ViewController.h
//  AlgoritmiMergeSort
//
//  Created by Oda Labs on 11/26/14.
//  Copyright (c) 2014 OdaLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textFieldInput;

@property (weak, nonatomic) IBOutlet UILabel *txtVektoriMajt;

@property (weak, nonatomic) IBOutlet UILabel *txtVektoriDjatht;

@property (weak, nonatomic) IBOutlet UILabel *txtRezultati;



@property (weak, nonatomic) IBOutlet UITextField *ant1;

@property (weak, nonatomic) IBOutlet UITextField *ant2;

@property (weak, nonatomic) IBOutlet UITextField *ant3;


@property (weak, nonatomic) IBOutlet UITextField *ant4;

@property (weak, nonatomic) IBOutlet UITextField *ant5;

@property (weak, nonatomic) IBOutlet UITextField *ant6;

@property (weak, nonatomic) IBOutlet UITextField *ant7;

@property (weak, nonatomic) IBOutlet UITextField *ant8;




- (IBAction)btnJep:(id)sender;

@end

